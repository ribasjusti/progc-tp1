# Mon premier programme :

## Compilation :
Dans un terminal ouvert à la racine du dossier "ribasjusti-tp1", tapez la commande : gcc -Wall premierProgramme.c -o premierProgramme

## Exécution :
De même, tapez la commande : ./premierProgramme

## Documentation doxygen :
Pour générer la documentation doxigen, tapez dans le terminal : doxygen Doxyfile
Pour accéder à la documentation vous pouvez par exemple aller dans le dossier "html" et ouvrir le fichier "index.html" dans un navigateur
