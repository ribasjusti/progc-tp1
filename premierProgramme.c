/*!
\file ex1.c
\brief le programme Hello World
\author Justine Ribas <ribasjusti@cy-tech.fr>
\version 0.1
\date 08 novembre 2021
*/

/*inclusion des entêtes de librairie*/
#include <stdio.h>
#include <stdlib.h>

/**
*\fn int main(int argc, char *argv[])
*\brief permet d'afficher un message
*\param argc nombre d'arguments
*\param argv valeur des arguments
*\return 0 si tout s'est bien passé
*\author Justine Ribas <ribasjusti@cy-tech.fr>
*\version 0.1
*\date 08 novembre 2021
*/
int main(int argc, char *argv[])
{
    //affichage du message
    printf("Bonjour tout le monde\n");
    //fin du programme : tout est OK
    return 0;
}

/*Question 3 :
\n : retour à la ligne
\t : tabulation
\v : retour à la ligne verticale
\b : supprime le dernier caractère
\r : retour chariot
\f : saut de page
\’ : apostrophe
\” : guillemet
\\ : barre oblique
\nnn : octet ASCII
\xnnn : octet hexa
*/